from django.test import TestCase, Client

# # Create your tests here.


class TestHome(TestCase):
    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'home/index.html')
