from django.urls import path

from . import views

app_name = 'home3'

urlpatterns = [
    path('', views.index, name='index'),
    path('table', views.table, name='table'),
    path('detail/<int:pk>/', views.detail, name='detail'),
    path('<int:pk>/', views.delete, name='hapus'),
]
