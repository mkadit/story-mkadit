from django import forms

from .models import Lessons


class Formulir(forms.Form):
    matkul = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Mata Kuliah',
        'type': 'text',
        'required': True,
        'class': 'input'
    }))

    dosen = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Dosen Mata Kuliah',
        'type': 'text',
        'required': True,
        'class': 'input'
    }))

    sks = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jumlah SKS',
        'type': 'text',
        'required': True,
        'class': 'input'
    }))

    description = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Sedikit mengenai Mata Kuliah',
        'type': 'text',
        'required': True,

        'class': 'input'
    }))

    semester_year = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Semester/tahun (E.x Gasal 2020/2021)',
        'type': 'text',
        'required': True,
        'class': 'input'

    }))

    kelas = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Ruangan matkul diajarkan',
        'type': 'text',
        'required': True,
        'class': 'input'

    }))
