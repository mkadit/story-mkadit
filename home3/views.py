from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.


def index(request):
    if(request.method == "POST"):
        context = forms.Formulir(request.POST)
        if(context.is_valid()):
            context1 = models.Lessons()
            context1.matkul = context.cleaned_data["matkul"]
            context1.dosen = context.cleaned_data["dosen"]
            context1.sks = context.cleaned_data["sks"]
            context1.description = context.cleaned_data["description"]
            context1.semester_year = context.cleaned_data["semester_year"]
            context1.kelas = context.cleaned_data["kelas"]
            context1.save()
        return redirect("/home3/")
    else:
        context = forms.Formulir()
        context1 = models.Lessons.objects.all()
        context_dictio = {
            'Formulir': context,
            'jadwal': context1
        }
    return render(request, 'home3/index.html', context_dictio)


def table(request):
    context = forms.Formulir()
    context1 = models.Lessons.objects.all()
    context_dictio = {
        'Formulir': context,
        'jadwal': context1
    }
    return render(request, 'home3/table.html', context_dictio)


def detail(request, pk):
    something = models.Lessons.objects.get(id=pk)
    context = {
        "pass_name": something
    }
    return render(request, 'home3/detail.html', context)


def delete(request, pk):
    if(request.method == "POST"):
        context = forms.Formulir(request.POST)
        if(context.is_valid()):
            context1 = models.Lessons()
            context1.matkul = context.cleaned_data["matkul"]
            context1.dosen = context.cleaned_data["dosen"]
            context1.sks = context.cleaned_data["sks"]
            context1.description = context.cleaned_data["description"]
            context1.semester_year = context.cleaned_data["semester_year"]
            context1.kelas = context.cleaned_data["kelas"]
            context1.save()
        return redirect("/home3/table")
    else:
        models.Lessons.objects.filter(pk=pk).delete()
        context = forms.Formulir()
        context1 = models.Lessons.objects.all()
        context_dictio = {
            'Formulir': context,
            'jadwal': context1
        }
    return render(request, 'home3/table.html', context_dictio)
