from django.db import models

# Create your models here.


class Lessons(models.Model):
    matkul = models.CharField(
        blank=False, max_length=100, help_text="Nama Mata Kuliah")
    dosen = models.CharField(blank=False, max_length=100,
                             help_text="Nama Dosen Mata Kuliah")
    sks = models.IntegerField(
        blank=False, help_text="Jumlah SKS")
    description = models.TextField(
        blank=False, max_length=200, help_text="Sedikit mengenai Mata Kuliah")
    semester_year = models.CharField(
        blank=False, max_length=100,
        help_text="Semester/tahun (E.x Gasal 2020/2021)")
    kelas = models.CharField(
        blank=False, max_length=100,
        help_text="Ruangan matkul diajarkan")
