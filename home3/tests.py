from django.test import TestCase, Client

# Create your tests here.


class TestHome3(TestCase):

    def test_url_index(self):
        response = Client().get('/home3/')
        self.assertEqual(response.status_code, 200)

    def test_template_index(self):
        response = Client().get('/home3/')
        self.assertTemplateUsed(response, 'home3/index.html')

    def test_url_table(self):
        response = Client().get('/home3/table')
        self.assertEqual(response.status_code, 200)

    def test_template_table(self):
        response = Client().get('/home3/table')
        self.assertTemplateUsed(response, 'home3/table.html')

    def test_post(self):
        response = Client().post('/home3/', data={'matkul': 'Hololive', 'dosen': 'Sui-chan',
                                                  'sks': '6', 'description': '1l0vesu1s31', 'semester_year': '2020', 'kelas': 'the world'})

    # def test_detail(self):
    #     response = Client().post('/home3/detail/0/')
