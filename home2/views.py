from django.shortcuts import render

# Create your views here.


def index(request):
    return render(request, 'home2/index.html')


def game(request):
    return render(request, 'home2/game.html')
