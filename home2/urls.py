from django.urls import path

from . import views

app_name = 'home2'

urlpatterns = [
    path('', views.index, name='index'),
    path('game', views.game, name='game'),
]
