from django.test import TestCase, Client

# Create your tests here.


class TestHome2(TestCase):
    def test_url_index(self):
        response = Client().get('/home2/')
        self.assertEqual(response.status_code, 200)

    def test_template_index(self):
        response = Client().get('/home2/')
        self.assertTemplateUsed(response, 'home2/index.html')

    def test_url_game(self):
        response = Client().get('/home2/game')
        self.assertEqual(response.status_code, 200)

    def test_template_game(self):
        response = Client().get('/home2/game')
        self.assertTemplateUsed(response, 'home2/game.html')
