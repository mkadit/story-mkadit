$('#search-form').submit(function name(e) {
	e.preventDefault();

	console.log($('#search-url').val());

	$.ajax({
		url: window.location.origin + '/home6/search',
		contentType: 'application/json',
		data: { q: $('#search-url').val() },
		dataType: 'json',
		success: function (result) {
			console.log(result);
			let parent = $('#books');
			let inner = '    <tr><th>Title</th><th>Author</th></tr>';
			let json = result.items;
			for (let i = 0; i < json.length; i++) {
				inner += '<tr>';
				inner += '<th>' + json[i].volumeInfo.title + '</th>';
				inner += '<th>' + json[i].volumeInfo.authors + '</th>';
				inner += '</tr>';
			}
			parent.html(inner);
		},
	});
});
