from django.test import Client, TestCase
from django.urls import reverse

# Create your tests here.


class TestHome6(TestCase):

    def test_url_index(self):
        response = Client().get('/home6/')
        self.assertEqual(response.status_code, 200)

    def test_template_index(self):
        response = Client().get('/home6/')
        self.assertTemplateUsed(response, 'home6/index.html')

    def test_url_search(self):
        response = Client().get('/home6/search')
        self.assertEqual(response.status_code, 200)

    def test_add_activity(self):
        response = Client().get(reverse('home6:search')+"?q=test", follow=True)
