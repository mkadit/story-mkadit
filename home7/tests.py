from django.test import Client, TestCase

# Create your tests here.


class TestHome7(TestCase):
    def test_url_index(self):
        response = Client().get('/home7/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/home7/')
        self.assertTemplateUsed(response, 'home7/register.html')

    def test_add_activity(self):
        response = Client().post('/home7/', data={'username': 'ilovesuise', 'email': 'ilovesuisei@suichan.com',
                                                  'password1': '1l0vesu1s31', 'password2': '1l0vesu1s31'})
