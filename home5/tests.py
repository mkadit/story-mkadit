from django.test import TestCase, Client

# Create your tests here.


class TestHome5(TestCase):
    def test_url(self):
        response = Client().get('/home5/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/home5/')
        self.assertTemplateUsed(response, 'home5/index.html')
