$(document).ready(function () {
	const allContents = $('.accordion_content').hide();
	$('.accordion_header').click(function (e) {
		console.log('show');
		e.preventDefault();

		var $this = $(this);

		if ($this.parent().next().hasClass('show')) {
			$this.parent().next().removeClass('show');
			$this.parent().next().slideUp(350);
		} else {
			$this.parent().next().toggleClass('show');
			$this.parent().next().slideToggle(350);
		}
	});

	$('.accordion_down').click(function (e) {
		console.log('clicked down');
		e.preventDefault();

		var $this = $(this).parent().parent();
		console.log($this);
		$this.next().insertBefore($this);
	});

	$('.accordion_up').click(function (e) {
		console.log('clicked up');
		e.preventDefault();

		var $this = $(this).parent().parent();
		console.log($this);
		$this.prev().insertAfter($this);
	});
});
