from django.shortcuts import render, redirect
from . import forms, models
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
@csrf_exempt
def index(request):
    if request.method == "POST":
        if 'activity_name' in request.POST:
            add_activity(request)
        elif 'participant_name' in request.POST and 'id_activity' in request.POST:
            add_participant(request)
    list_all = list_creation(request)

    temp_dict = {
        'activity_form': forms.CreateActivityForm,
        'participant_form': forms.CreateParticipantForm,
        'data': list_all
    }

    return render(request, 'home4/index.html', temp_dict)


def add_activity(request):
    activity_form = forms.CreateActivityForm(request.POST)
    if activity_form.is_valid():
        temp = models.Activity()
        temp.activity_name = activity_form.cleaned_data['activity_name']
        temp.save()
        return redirect('/home4/')


def add_participant(request):
    participant_form = forms.CreateParticipantForm(request.POST)
    if participant_form.is_valid():
        models.Activity.objects.get(id=request.POST['id_activity'])
        temp = models.Participant()
        temp.participant_name = participant_form.cleaned_data['participant_name']
        temp.activity = models.Activity.objects.get(
            id=request.POST['id_activity'])
        temp.save()
        return redirect('/home4/')


def list_creation(request):
    list_kegiatan = models.Activity.objects.all()
    list_all = []
    for activity in list_kegiatan:
        orang = models.Participant.objects.filter(activity=activity)
        list_orang = []
        for i in orang:
            list_orang.append(i)
        list_all.append((activity, list_orang))
    return list_all
