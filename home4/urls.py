from django.urls import path

from . import views

app_name = 'home4'

urlpatterns = [
    path('', views.index, name='index'),
]
