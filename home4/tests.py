from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Activity, Participant

# Create your tests here.


class TestHome4(TestCase):
    def setUp(self):
        Activity.objects.create(activity_name='Tetris')

    def test_url(self):
        response = Client().get('/home4/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/home4/')
        self.assertTemplateUsed(response, 'home4/index.html')

    def test_activity_model(self):
        Activity.objects.create(activity_name='Raid Red Chocobo')
        count = Activity.objects.all().count()
        self.assertEqual(count, 2)

    def test_participant_model(self):
        Participant.objects.create(participant_name='Moogle King')
        count = Participant.objects.all().count()
        self.assertEqual(count, 1)

    def test_activity_name_model(self):
        Activity.objects.create(activity_name='Pasrah')
        activity = Activity.objects.get(activity_name='Pasrah')
        self.assertEqual(str(activity), 'Pasrah')

    def test_participant_name_model(self):
        Participant.objects.create(participant_name='Mr.Pasrah')
        participant = Participant.objects.get(participant_name='Mr.Pasrah')
        self.assertEqual(str(participant), 'Mr.Pasrah')

    def test_add_activity(self):
        activity_num = Activity.objects.all().count()
        response = Client().post('/home4/', data={'activity_name': 'Titania'})
        activity = Activity.objects.get(activity_name='Titania')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Activity.objects.all().count(), activity_num+1)

    def test_add_participant(self):
        participant_num = Participant.objects.all().count()
        response = Client().post(
            '/home4/', data={'participant_name': 'Klee', 'id_activity': '1'})
        participant = Participant.objects.get(participant_name='Klee')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Participant.objects.all().count(), participant_num+1)
