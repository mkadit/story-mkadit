from django import forms
from .models import Activity, Participant


class CreateActivityForm(forms.Form):
    activity_name = forms.CharField(label='Nama Kegiatan ', max_length=30, required=True,
                                    widget=forms.TextInput(
                                        attrs={'class': 'form-control'})
                                    )


class CreateParticipantForm(forms.Form):
    participant_name = forms.CharField(label='Nama Peserta ', max_length=50, required=True,
                                       widget=forms.TextInput(
                                           attrs={'class': 'form-control'})
                                       )
