from django.db import models

# Create your models here.


class Activity(models.Model):
    activity_name = models.CharField(max_length=50, default="")

    def __str__(self):
        return self.activity_name


class Participant(models.Model):
    activity = models.ForeignKey(
        Activity, on_delete=models.DO_NOTHING, null=True)
    participant_name = models.CharField(max_length=50)

    def __str__(self):
        return self.participant_name
